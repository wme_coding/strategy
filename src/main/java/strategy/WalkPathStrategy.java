package strategy;

import distance.Path;

public class WalkPathStrategy implements Strategy{
    @Override
    public Path getPath() {
        return new Path("For Walk");
    }
}
