package strategy;

import distance.Path;

public class BusPathStrategy implements Strategy{
    @Override
    public Path getPath() {
        return new Path("For Bus");
    }
}
