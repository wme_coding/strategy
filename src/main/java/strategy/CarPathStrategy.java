package strategy;

import distance.Path;

public class CarPathStrategy implements Strategy{
    @Override
    public Path getPath() {
        return new Path("For Car");
    }
}
