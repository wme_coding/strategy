package strategy;

import distance.Path;

public interface Strategy {
    Path getPath();
}
