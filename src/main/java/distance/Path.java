package distance;

public class Path {
    private String pathType;

    public Path(String pathType) {
        this.pathType = pathType;
    }

    public String getPathType() {
        return pathType;
    }

    //The direction to the aim point
}
