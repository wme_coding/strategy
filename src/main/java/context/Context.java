package context;

import distance.Path;
import strategy.Strategy;

public class Context {
    private Strategy strategy;

    public Context(Strategy strategy) {
        this.strategy = strategy;
    }

    public Context() {
        strategy = null;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public Path getPath(){
        return strategy.getPath();
    }
}
