import context.Context;
import distance.Path;
import org.junit.Assert;
import org.junit.Test;
import strategy.BusPathStrategy;
import strategy.CarPathStrategy;
import strategy.WalkPathStrategy;

public class TestNavigator {

    @Test
    public void testBusPath(){
        Context context = new Context();
        //here we can select our action
        context.setStrategy(new BusPathStrategy());
        Path result = context.getPath();
        Assert.assertEquals("For Bus", result.getPathType());
    }

    @Test
    public void testCarPath(){
        Context context = new Context();
        //here we can select our action
        context.setStrategy(new CarPathStrategy());
        Path result = context.getPath();
        Assert.assertEquals("For Car", result.getPathType());
    }

    @Test
    public void testWalkPath(){
        Context context = new Context();
        //here we can select our action
        context.setStrategy(new WalkPathStrategy());
        Path result = context.getPath();
        Assert.assertEquals("For Walk", result.getPathType());
    }
}
